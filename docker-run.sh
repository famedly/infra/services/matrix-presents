#!/bin/sh
if [ ! -f "/data/config.json" ]; then
        echo "No config found"
        exit 1
fi
yarn serve
