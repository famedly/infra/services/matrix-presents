# matrix-presents

A presentation client that reads from a Matrix room and displays it as pretty slides!

For help and support, visit [#presents:half-shot.uk](https://matrix.to/#/#presents:half-shot.uk)

## Project setup

To run the project:

```
git clone https://github.com/Half-Shot/matrix-presents
cd matrix-presents
yarn
```
Create `config.json` (e.g. based on `config.sample.json`) then
```
yarn serve
```

## Container setup

```
git clone https://github.com/Half-Shot/matrix-presents
docker build -t matrix-presents .
```
Create `config.json` (e.g. based on `config.sample.json`) then
```
docker run -v `pwd`/config.json:/data/config.json -p 8080:8080 matrix-presents
```

## Docs

- [Slide Theory](docs/slide-theory.md) contains some information about how Presents uses Matrix events and rooms.
